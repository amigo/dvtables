new Vue({
    el: 'body',
    data: function () {
        return {
            count: 0,
            description: null,
            member: '',
            messages: [],
            messageType: '',
            messageTypes: [],
            next: null,
            order: 'created',
            order_direction: '-',
            page: 1,
            previous: null,
            search: '',
            status: 'all',
            url: '/api/messages/',
        }
    },
    ready: function () {
        this.getMessages(this.url);
    },
    methods: {
        getMessages: function () {
            var url = this.url + '?' + this.getFilters();
            this.$http.get(url).then(function (response) {
                this.$set('messages', response.data.results);
                this.$set('next', response.data.has_next);
                this.$set('previous', response.data.has_previous);
                this.$set('count', response.data.count);
                this.$set('page', response.data.page);
            })

        },
        getFilters: function () {
            return $.param({
                member: this.member,
                message_type: this.messageType,
                message_types: this.messageTypes,
                order: this.order,
                order_direction: this.order_direction,
                page: this.page,
                search: this.search,
                status: this.status,
            })
        },
        nextPage: function () {
            if (this.next) {
                this.page += 1;
                this.getMessages();
            }
        },
        prevPage: function () {
            if (this.previous) {
                this.page -= 1;
                this.getMessages();
            }
        },
        changeOrder: function (value) {
            var same = this.order == value;
            var reverse = this.order_direction == '' ? '-' : '';
            this.order_direction =  same ? reverse : '';
            this.order = value;
            this.page = 1;
            this.getMessages(this.getFilters());
        },
        getCaretClass: function (order) {
            if (this.order != order) {
                return '';
            }
            return this.order_direction == '' ? 'fa-caret-down' : 'fa-caret-up';
        },
        toggleDescription: function (id) {
            var same = this.description == id;
            this.description = same ? null: id;
            console.log(id, this.description);
        },
        isHidden: function (id) {
            
            return this.description == id;
        }
    },
    watch: {
        'search': function () {
            this.page = 1;
            this.getMessages(this.getFilters());
        },
        'status': function () {
            this.page = 1;
            this.getMessages(this.getFilters());
        },
        'messageTypes': function () {
            this.page = 1;
            this.getMessages(this.getFilters());
        },
        'messageType': function () {
            this.page = 1;
            this.getMessages(this.getFilters());
        },
        'member': function () {
            this.page = 1;
            this.getMessages(this.getFilters());
        },
        'order': function () {
            this.page = 1;
            this.getMessages(this.getFilters());
        },
    }
});
