from fabric.api import *

HOME = '~/'
PROJECT = '~/project'
PIP = '~/env/bin/pip'
PID = '~/django.pid'
MANAGE = '~/env/bin/python ~/project/manage.py'
SOURCE = 'git@bitbucket.org:amigo/dvtables.git'

env.always_use_pty = False
env.forward_agent = True

STAGES = {
    'default': {
        'user': 'dvtables',
        'hosts': ['37.46.134.71'],
        'branch': 'master',
    },
}


def stage_set(stage_name='default'):
    env.stage = stage_name
    for option, value in STAGES[env.stage].items():
        setattr(env, option, value)


@task
def default():
    stage_set('default')


@task
def uname():
    run('uname -a')


@task
def update_project():
    with cd(PROJECT):
        run('git fetch origin %s' % env.branch)
        run('git reset --hard FETCH_HEAD')
        run('git clean -df')


@task
def update_env():
    run('%s install -U -r %s/requirements.txt' % (PIP, PROJECT))


@task
def update_settings():
    run('cp /etc/settings/dvtables.py %s/project/settings.py' % PROJECT)


@task
def collectstatic():
    with cd(PROJECT):
        run('%s collectstatic --noinput' % (MANAGE,))


@task
def start():
    with cd(PROJECT):
        run('~/env/bin/gunicorn -c gunicorn.conf.py project.wsgi')


@task
def stop():
    run('kill `cat %s`' % PID)


@task
def restart():
    run('kill -HUP `cat %s`' % PID)


@task
def cronjob():
    with cd(PROJECT):
        run('crontab cronjob')


@task
def migrate():
    with cd(PROJECT):
        run('%s migrate --no-input' % (MANAGE,))


@task
def ping_google():
    with cd(PROJECT):
        run('%s ping_google' % (MANAGE,))


@task
def compilemessages():
    with cd(PROJECT):
        run('%s compilemessages' % (MANAGE,))


@task
def createsuperuser():
    with cd(PROJECT):
        run('%s createsuperuser' % (MANAGE,))


@task
def changepassword():
    with cd(PROJECT):
        username = prompt('Type username:')
        run('%s changepassword %s' % (MANAGE, username))


@task
def install():
    with cd(HOME):
        run('rm -rf project')
        run('mkdir -p project env media/editor media/static static')
        run('/usr/local/bin/virtualenv --clear --no-site-packages env')
        run('ssh-keyscan bitbucket.org >> ~/.ssh/known_hosts')
        run('git clone -b %s -- %s project' % (env.branch, SOURCE))
    update_settings()
    update_env()


@task
def update():
    update_project()
    update_settings()
    migrate()
    collectstatic()
    # compilemessages()
    cronjob()
    restart()


@task
def upgrade():
    update_project()
    update_settings()
    update_env()
    migrate()
    collectstatic()
    # compilemessages()
    cronjob()
    stop()
    start()
