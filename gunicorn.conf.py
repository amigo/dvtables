from os.path import expanduser


bind = 'unix:/home/dvtables/django.socket'
pidfile = expanduser('~/django.pid')
accesslog = expanduser('~/access.log')
errorlog = expanduser('~/error.log')
daemon = True
workers = 2
