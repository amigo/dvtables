from django.views.generic import TemplateView

from application.models import MessageType


class Messages(TemplateView):
    template_name = 'messages.html'

    def get_context_data(self, **kwargs):
        context = super(Messages, self).get_context_data(**kwargs)
        context.update(
            message_types=MessageType.objects.all(),
        )
        return context
