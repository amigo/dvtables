from __future__ import unicode_literals

from django.db import models
from django.utils import timezone
from django.utils.six import python_2_unicode_compatible


@python_2_unicode_compatible
class Member(models.Model):
    SEX = (
        ('M', 'Male'),
        ('F', 'Female'),
    )
    company = models.ForeignKey('application.Company')
    name = models.CharField(max_length=96, db_index=True)
    birthday = models.DateField()
    sex = models.CharField(db_index=True, max_length=1, choices=SEX,)
    email = models.EmailField()

    class Meta:
        verbose_name = 'member'
        verbose_name_plural = 'members'

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class Company(models.Model):
    title = models.CharField(max_length=64, db_index=True)

    class Meta:
        verbose_name = 'company'
        verbose_name_plural = 'companies'

    def __str__(self):
        return self.title


@python_2_unicode_compatible
class MessageType(models.Model):
    title = models.CharField(max_length=128, db_index=True)

    class Meta:
        verbose_name = 'message type'
        verbose_name_plural = 'message types'

    def __str__(self):
        return self.title


@python_2_unicode_compatible
class Message(models.Model):
    member = models.ForeignKey('application.Member', null=True)
    message_type = models.ForeignKey('application.MessageType', null=True)
    resolved = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)
    changed = models.DateTimeField(default=timezone.now)

    class Meta:
        verbose_name = 'message'
        verbose_name_plural = 'messages'

    def __str__(self):
        return self.id
