from django.utils.formats import date_format
from rest_framework.serializers import ModelSerializer, SerializerMethodField, \
    timezone

from application.models import Message, MessageType, Company, Member


class CompanySerializer(ModelSerializer):
    class Meta:
        model = Company


class MemberSerializer(ModelSerializer):
    age = SerializerMethodField()
    company = CompanySerializer()
    sex = SerializerMethodField()

    class Meta:
        model = Member

    def get_sex(self, obj):
        return obj.get_sex_display()

    def get_age(self, obj):
        return (timezone.now().date() - obj.birthday).days / 365


class MessageTypeSrializer(ModelSerializer):
    class Meta:
        model = MessageType


class MessageSerializer(ModelSerializer):
    member = MemberSerializer()
    message_type = MessageTypeSrializer()
    created = SerializerMethodField()

    class Meta:
        model = Message

    def get_created(self, obj):
        return date_format(obj.created, 'SHORT_DATETIME_FORMAT')
