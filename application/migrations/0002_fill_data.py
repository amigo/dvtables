# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import random

from datetime import datetime, timedelta
from django.db import migrations
from faker import Faker

fake = Faker()


def fill_company(apps, schema_editor):
    company = apps.get_model('application', 'Company')
    for n in xrange(20):
        company.objects.create(
            title=fake.company()
        )


def fill_members(apps, schema_editor):
    member = apps.get_model('application', 'Member')
    company = apps.get_model('application', 'Company')
    companies = company.objects.values_list('id', flat=True)
    for n in xrange(100):
        profile = fake.profile()
        member.objects.create(
            sex=profile['sex'],
            name=profile['name'],
            company_id=random.choice(companies),
            birthday=fake.date_time_between_dates(datetime.now() - timedelta(days=36000), datetime.now() - timedelta(days=3000)),
            email=profile['mail']
        )


def fill_messages_types(apps, schema_editor):
    message_type = apps.get_model('application', 'MessageType')
    message_type.objects.bulk_create([
        message_type(title='Complaint'),
        message_type(title='Greetings'),
        message_type(title='Appreciation'),
        message_type(title='Declaration'),
        message_type(title='Petition'),
        message_type(title='Abrogation'),
    ])


def fill_messages(apps, schema_editor):
    message = apps.get_model('application', 'Message')
    member = apps.get_model('application', 'Member')
    message_type = apps.get_model('application', 'MessageType')
    members = member.objects.values_list('id', flat=True)
    message_types = message_type.objects.values_list('id', flat=True)
    for n in xrange(10000):
        message.objects.create(
            member_id=random.choice(members),
            message_type_id=random.choice(message_types),
            resolved=random.choice([True, False]),
            created=fake.date_time_between_dates(datetime.now() - timedelta(days=36000), datetime.now() - timedelta(days=3000))
        )


class Migration(migrations.Migration):
    dependencies = [
        ('application', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(fill_company, migrations.RunPython.noop),
        migrations.RunPython(fill_members, migrations.RunPython.noop),
        migrations.RunPython(fill_messages_types, migrations.RunPython.noop),
        migrations.RunPython(fill_messages, migrations.RunPython.noop),
    ]
