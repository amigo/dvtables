from collections import OrderedDict

from django.db.models import Q
from rest_framework.generics import ListAPIView
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response

from application.models import Message
from application.serializers import MessageSerializer


class MessagePagination(PageNumberPagination):
    page_size = 10

    def get_paginated_response(self, data):
        return Response(OrderedDict([
            ('count', self.page.paginator.count),
            ('has_next', self.page.has_next()),
            ('has_previous', self.page.has_previous()),
            ('page', self.page.number),
            ('results', data)
        ]))


class MessageListAPIView(ListAPIView):
    queryset = Message.objects.select_related().prefetch_related(
        'member__company')
    serializer_class = MessageSerializer
    pagination_class = MessagePagination

    def filter_queryset(self, queryset):
        queryset = super(MessageListAPIView, self).filter_queryset(queryset)
        query = self.request.query_params

        message_type = query.getlist('message_types[]', None)
        if message_type:
            queryset = queryset.filter(message_type__in=message_type)

        status = query.get('status', 'all')
        if not status == 'all':
            resolve = status == 'resolved' and True or False
            queryset = queryset.filter(resolved=resolve)

        search = query.get('search', '')
        if search:
            queryset = queryset.filter(
                Q(member__name__icontains=search) |
                Q(member__company__title__icontains=search),
            )

        member = query.get('member', '')
        if member:
            queryset = queryset.filter(
                member__name__icontains=member,
            )

        message_type = query.get('message_type', '')
        if message_type:
            queryset = queryset.filter(
                message_type__title__icontains=message_type,
            )

        order = query.get('order', 'member__name')
        order_direction = query.get('order_direction', '')
        queryset = queryset.order_by(order_direction + order)
        return queryset
